﻿using System;
using System.IO;
using System.Numerics;
using System.Collections.Generic;

class RSAEncryption
{
    public static void Main(string[] args)
    {
        Console.WriteLine("Welcome to the RSA Algorithm Encryption/Decryption System ");
        Console.WriteLine();
        Console.WriteLine("Enter two prime numbers (p and q): ");
        BigInteger p = ReadPrime("p");
        BigInteger q = ReadPrime("q");

        BigInteger n = p * q;

        BigInteger phi = (p - 1) * (q - 1);

        BigInteger e = ChoosePublicExponent(phi);

        Console.WriteLine("Public Key (n, e): (" + n + ", " + e + ")");
        Console.WriteLine("Private Key (n, d): (" + n + ", " + ModInverse(e, phi) + ")");

        SavePublicKeyToFile(n, e);
        Console.WriteLine();

        Console.WriteLine("Enter the plaintext (text): ");
        string plaintext = Console.ReadLine();

        BigInteger[] encryptedText = EncryptText(plaintext, n, e);
        SaveCiphertextToFile(encryptedText);

        BigInteger[] readCiphertext = ReadCiphertextFromFile();
        Console.WriteLine("Read Ciphertext from file: " + string.Join(" ", readCiphertext));

        string decryptedText = DecryptText(readCiphertext, n);
        Console.WriteLine("Decrypted Plaintext: " + decryptedText);
    }

    static BigInteger ReadPrime(string name)
    {
        BigInteger prime;
        do
        {
            Console.Write($"Enter prime number {name}: ");
        } while (!BigInteger.TryParse(Console.ReadLine(), out prime) || !IsPrime(prime));
        return prime;
    }

    static bool IsPrime(BigInteger number)
    {
        if (number <= 1) return false;
        if (number == 2 || number == 3) return true;
        if (number % 2 == 0 || number % 3 == 0) return false;
        for (BigInteger i = 5; i * i <= number; i += 6)
        {
            if (number % i == 0 || number % (i + 2) == 0)
                return false;
        }
        return true;
    }

    static BigInteger ChoosePublicExponent(BigInteger phi)
    {
        BigInteger e = 65537;
        while (EuclideanGCD(e, phi) != 1)
        {
            e++;
            if (e >= phi)
            {
                throw new Exception("Failed to find a suitable public exponent.");
            }
        }
        return e;
    }

    static BigInteger EuclideanGCD(BigInteger a, BigInteger b)
    {
        while (b != 0)
        {
            BigInteger temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    static BigInteger ModInverse(BigInteger a, BigInteger m)
    {
        BigInteger m0 = m;
        BigInteger y = 0, x = 1;

        while (a > 1)
        {
            BigInteger q = a / m;
            BigInteger t = m;

            m = a % m;
            a = t;
            t = y;

            y = x - q * y;
            x = t;
        }

        if (x < 0)
            x += m0;

        return x;
    }

    static BigInteger[] EncryptText(string plaintext, BigInteger n, BigInteger e)
    {
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(plaintext);
        BigInteger[] encrypted = new BigInteger[bytes.Length];
        for (int i = 0; i < bytes.Length; i++)
        {
            encrypted[i] = BigInteger.ModPow(bytes[i], e, n);
        }
        return encrypted;
    }

    static void SavePublicKeyToFile(BigInteger n, BigInteger e)
    {
        string publicKey = n + "," + e;
        File.WriteAllText("public_key.txt", publicKey);
        Console.WriteLine();
        Console.WriteLine("Public key saved to file.");
    }

    static void SaveCiphertextToFile(BigInteger[] ciphertext)
    {
        string ciphertextString = string.Join(" ", ciphertext);
        File.WriteAllText("ciphertext.txt", ciphertextString);
        Console.WriteLine();
        Console.WriteLine("Ciphertext saved to file.");
    }

    static BigInteger[] ReadCiphertextFromFile()
    {
        string ciphertextString = File.ReadAllText("ciphertext.txt");
        string[] ciphertextArray = ciphertextString.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
        BigInteger[] ciphertext = new BigInteger[ciphertextArray.Length];
        for (int i = 0; i < ciphertextArray.Length; i++)
        {
            ciphertext[i] = BigInteger.Parse(ciphertextArray[i]);
        }
        return ciphertext;
    }

    static string DecryptText(BigInteger[] ciphertext, BigInteger n)
    {
        BigInteger[] factors = Factorize(n);
        if (factors.Length != 2)
        {
            throw new Exception("Failed to factorize n into two prime numbers.");
        }
        BigInteger p = factors[0];
        BigInteger q = factors[1];
        BigInteger phi = (p - 1) * (q - 1);
        BigInteger e = ChoosePublicExponent(phi);
        BigInteger d = ModInverse(e, phi);

        byte[] decryptedBytes = new byte[ciphertext.Length];
        for (int i = 0; i < ciphertext.Length; i++)
        {
            BigInteger decryptedValue = BigInteger.ModPow(ciphertext[i], d, n);
            decryptedBytes[i] = (byte)((decryptedValue % n + n) % n);
        }
        return System.Text.Encoding.UTF8.GetString(decryptedBytes);
    }

    static BigInteger[] Factorize(BigInteger n)
    {
        List<BigInteger> factors = new List<BigInteger>();
        for (BigInteger i = 2; i <= n / i; i++)
        {
            while (n % i == 0)
            {
                factors.Add(i);
                n /= i;
            }
        }
        if (n > 1)
        {
            factors.Add(n);
        }
        return factors.ToArray();
    }
}
